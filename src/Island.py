from colour import Color

class Island():
    def __init__(self):
        pass

    def makeGradient(self, iterations):
        tan = Color('tan')
        lightyellow = Color("#ffffe0")
        lightgreen = Color('lightgreen')
        green = Color('green')


        grad = list(tan.range_to(lightyellow, int(iterations/6)))
        grad += list(lightyellow.range_to(lightgreen, int(iterations/6)))[1:]
        grad += list(lightgreen.range_to(green, int(iterations/6)))[1:]
        grad += list(green.range_to(lightgreen, int(iterations/6)))[1:]
        grad += list(lightgreen.range_to(lightyellow, int(iterations/6)))[1:]
        x = iterations - len(grad)
        grad += list(lightyellow.range_to(tan, x + 1))[1:]
        print(len(grad))
        return grad
