from colour import Color

class Rainbow():
    def __init__(self):
        pass

    def makeGradient(self, iterations):
        grad = []
        for i in range(iterations):
            if i % 6 == 0:
                grad.append(Color('red'))
            elif i % 6 == 1:
                grad.append(Color('orange'))
            elif i % 6 == 2:
                grad.append(Color('yellow'))
            elif i % 6 == 3:
                grad.append(Color('green'))
            elif i % 6 == 4:
                grad.append(Color('blue'))
            else:
                grad.append(Color('purple'))
        return grad
