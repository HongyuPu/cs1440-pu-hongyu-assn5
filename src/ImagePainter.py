from tkinter import Tk, Canvas, PhotoImage, mainloop
import Config
import FractalFactory
import GradientFactory


'''
replaced z var with self.z = 0
replaced global max its var with len(gradient
removed global i var
introduced parameter gradient into colorOfThePixel

'''
class ImagePainter():
    def __init__(self):
        self.gradient = GradientFactory.GradientFactory()
        self.config = Config.Config()
        self.fractal = FractalFactory.FractalFactory()
        pass

    def paint(self, configFile, gradientName):
        self.config = self.config.getConfig(configFile)
        self.gradient = self.gradient.makeGradient(gradientName, self.config['iterations'])  # makes a gradient
        pixels = self.config['pixels']

        window = Tk()

        img = PhotoImage(width=pixels, height=pixels)
        minx = self.config['centerx'] - (self.config['axislength'] / 2.0)
        maxx = self.config['centerx'] + (self.config['axislength'] / 2.0)
        miny = self.config['centery'] - (self.config['axislength'] / 2.0)

        canvas = Canvas(window, width=pixels, height=pixels, bg=self.gradient[0])  # replaced gradient
        canvas.pack()
        canvas.create_image((pixels / 2, pixels / 2), image=img, state="normal")

        pixelSize = abs(maxx - minx) / pixels

        for row in range(pixels, 0, -1):
            for col in range(pixels):
                x = minx + col * pixelSize
                y = miny + row * pixelSize
                if "julia" in self.config['type']:
                    colorInt = self.fractal.makeJulia(complex(x, y), self.config['creal'], self.config['cimag'],
                                                   self.config['power'], self.config['iterations'])
                else:
                    colorInt = self.fractal.makeMandelbrot(complex(x, y), self.config['power'], self.config['iterations'])
                img.put(self.gradient[colorInt - 1], (col, pixels - row))
            window.update()
        mainloop()