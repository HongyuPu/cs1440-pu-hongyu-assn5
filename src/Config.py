
class Config():
    def __init__(self):
        self.config = dict()
        pass

    def isfloat(self, value):
        try:
            float(value)
            return True
        except ValueError:
            return False

    def getConfig(self, frac):
        file = open(frac)
        text = file.readlines()
        for i in text:
            line = i.split(": ")
            if len(line) == 2:
                line[1] = line[1].strip()
                if line[1].isnumeric():
                    self.config[line[0]] = int(line[1])
                elif self.isfloat(line[1]):
                    self.config[line[0]] = float(line[1])
                else:
                    self.config[line[0]] = line[1]
        file.close()
        power = list(self.config['type'])
        if power[len(power) - 1].isdigit():
            self.config['power'] = int(power[len(power) - 1])
        else:
            self.config['power'] = int(2)
        return self.config