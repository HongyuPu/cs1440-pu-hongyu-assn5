from colour import Color

class Default():
    def __init__(self):
        pass

    def makeGradient(self, iterations):  # Example gradient
        red = Color('red')
        grn = Color('green')
        blu = Color('blue')
        blk = Color('black')

        grad = list(red.range_to(grn, int(iterations/5)))

        grad += list(blk.range_to(grn, int(iterations/5)))[1:]

        grad += list(grn.range_to(blk, int(iterations/5)))[1:]
        grad += list(grn.range_to(blu, int(iterations/5)))[1:]
        x = iterations - len(grad)

        grad += list(blu.range_to(red, x))
        return grad