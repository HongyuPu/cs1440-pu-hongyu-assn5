from colour import Color

class BlackWhite():  # instructor's simple gradient
    def __init__(self):
        self.colors = []

    def makeGradient(self, iterations):
        for i in range(iterations):
            if i % 2 == 0:
                self.colors.append(Color('white'))
            else:
                self.colors.append(Color('black'))
        return self.colors