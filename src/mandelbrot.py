import Fractal


class mandelbrot():
    def __init__(self, getCount = Fractal.Fractal()):
        self.getCount = getCount
        pass

    def getColor(self, c, power, maximum):  # should be obsolete
        """Return the color of the current pixel within the Mandelbrot set"""
        z = complex(0, 0)  # z0
        return self.getCount.getCountComplex(z, c, power, maximum)
        # for i in range(len(gradient)):
        #     z = z * z + c  # Get z1, z2, ...
        #     if abs(z) > 2:
        #         z = 2.0
        #         return gradient[i]
        # return gradient[len(gradient) - 1]
    def makeFractal(self, c, power, maximum):
        """Return an integer to be used in a gradient"""
        z = complex(0, 0)  # z0
        return self.getCount.getCountComplex(z, c, power, maximum)