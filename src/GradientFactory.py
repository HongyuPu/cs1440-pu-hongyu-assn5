import BlackWhite
import DefaultRandom
import Default
import Snow
import Rainbow
import Island

class GradientFactory():
    def __init__(self):
        pass

    def makeGradient(self, name, iterations):
        if name == 'BlackWhite':
            gradient = BlackWhite.BlackWhite()
            return gradient.makeGradient(iterations)
        elif name == 'Default':
            gradient = Default.Default()
            return gradient.makeGradient(iterations)
        elif name == 'Snow':
            gradient = Snow.Snow()
            return gradient.makeGradient(iterations)
        elif name == "Rainbow":
            gradient = Rainbow.Rainbow()
            return gradient.makeGradient(iterations)
        elif name == 'Island':
            gradient = Island.Island()
            return gradient.makeGradient(iterations)
        else:
            gradient = DefaultRandom.DefaultRandom()
            return gradient.makeGradient(iterations)