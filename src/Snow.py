from colour import Color

class Snow():
    def __init__(self):
        pass

    def makeGradient(self, iterations):
        white = Color("white")
        green = Color("green")
        brown = Color("brown")

        grad = list(white.range_to(green, int(iterations/3)))
        grad += list(green.range_to(brown, int(iterations/3)))[1:]
        x = iterations - len(grad)
        grad += list(brown.range_to(white, x))
        return grad

