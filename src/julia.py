import Fractal


class julia():
    def __init__(self, getCount = Fractal.Fractal()):
        self.getCount = getCount
        pass

    def getColor(self, z, creal, cimag, power, maximum):  # should be obsolete
        """Return the index of the color of the current pixel within the Julia set
        in the gradient array"""
        # creal = config['creal']
        # cimag = config['cimag']
        c = complex(creal, cimag)
        return self.getCount.getCountComplex(z, c, power, maximum)
        # for i in range(len(gradient)):  # 78
        #     z = z * z + c  # Iteratively compute z1, z2, z3 ...
        #     if abs(z) > 2:
        #         return gradient[i]  # The sequence is unbounded
        # return gradient[len(gradient) - 1]  # Else this is a bounded sequence, 77
    def makeFractal(self, z, creal, cimag, power, maximum):
        """Return an integer to be used in gradient"""
        c = complex(creal, cimag)
        return self.getCount.getCountComplex(z, c, power, maximum)