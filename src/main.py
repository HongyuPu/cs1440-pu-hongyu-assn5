import sys
import ImagePainter


fractal = ImagePainter.ImagePainter()

if len(sys.argv) == 3:
    fractal.paint(sys.argv[1], sys.argv[2])
if len(sys.argv) == 2:
    fractal.paint(sys.argv[1], 'default')
else:
    print("USAGE: main.py [FILE] [GRADIENT NAME]")