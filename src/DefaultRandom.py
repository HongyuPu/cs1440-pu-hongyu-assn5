from colour import Color
import random

class DefaultRandom():
    def __init__(self):
        pass

    def makeFloatRand(self, start, end):
        f = float(random.randint(start, end))
        return f/100

    def makeGradient(self, iterations):
        start = Color(rgb=(self.makeFloatRand(0, 100), self.makeFloatRand(0, 100), self.makeFloatRand(0, 100)))
        end = Color(rgb=(self.makeFloatRand(0, 100), self.makeFloatRand(0, 100), self.makeFloatRand(0, 100)))
        return list(start.range_to(end, iterations))
