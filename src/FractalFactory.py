import mandelbrot
import julia

class FractalFactory():
    def __init__(self, mandelbrot = mandelbrot.mandelbrot(), julia = julia.julia(), config = dict()):
        self.config = config
        self.mandelbrot = mandelbrot
        self.julia = julia
        pass

    def makeJulia(self, c, creal, cimag, power, iterations):
        return self.julia.makeFractal(c, creal, cimag, power, iterations)

    def makeMandelbrot(self, c, power, iterations):
        return self.mandelbrot.getColor(c, power, iterations)

    def makeFractal(self, c): # abstract function now
        raise NotImplementedError("Abstract Method")


