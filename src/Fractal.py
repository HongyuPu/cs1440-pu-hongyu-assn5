

class Fractal():
    def __init__(self):
        pass

    def getCountComplex(self, z, c, power, maximum):
        for i in range(maximum):
            z = z ** power + c
            if abs(z) > 2:
                return i
        return maximum

    def makeFractal(self):
        raise NotImplementedError("Abstract Method")
