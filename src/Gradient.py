class Gradient():
    def __init__(self):
        self.colors = []
        self.gradient = []

    def getColor(self, integer):  # makeGradient black and white -- put into new file
        if integer.isnumeric():
            return self.gradient[integer]
        else:
            raise IOError("getColor input is not numberic")


    def makeGradient(self):
        raise NotImplementedError("Abstract Method")