for use to display:     python src/main.py [FRACTAL FILE] [GRADIENT NAME]
If no gradient is given, then it will use the a randomly generated gradient.
If no file is given, it will display usage.